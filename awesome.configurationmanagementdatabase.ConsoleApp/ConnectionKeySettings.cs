﻿namespace awesome.configurationmanagementdatabase.ConsoleApp
{
    public class ConnectionKeySettings
    {
        public string AwsAccessKeyId { get; set; }
        public string AwsSecretKey { get; set; }
        public string AlibabaAccessKeyId { get; set; }
        public string AlibabaSecretKey { get; set; }
    }
}