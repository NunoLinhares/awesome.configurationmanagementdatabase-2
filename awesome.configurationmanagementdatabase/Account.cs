﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace awesome.configurationmanagementdatabase
{
    public class Account
    {
        public string DataCentreType { get; set; }
        public string AccountName { get; set; }
        public string AccountId { get; set; }
        public List<ServerGroup> ServerGroups { get; set; } = new List<ServerGroup>();
        public List<CloudUser> Users { get; set; } = new List<CloudUser>();
        public List<CloudVolume> Volumes { get; set; } = new List<CloudVolume>();
        public List<CloudDatabase> Databases { get; set; } = new List<CloudDatabase>();
    }

    public class CloudDatabase
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Engine { get; set; }
        public string Version { get; set; }
        public string AvailabilityZone { get; set; }
    }

    public class ServerGroup
    {
        public string GroupName { get; set; }
        public string GroupId { get; set; }
        public string Region { get; set; }
        public List<ServerDetails> Servers { get; set; } = new List<ServerDetails>();
    }

    public class CloudUser
    {
        public string Id { get; set; }
        public string User { get; set; }
        public string Email { get; set; }
        public bool? Active { get; set; } = null;
        public DateTime? CreateDate { get; set; } = null;
        public DateTime? UpdateDate { get; set; } = null;
        public DateTime? PasswordLastUsed { get; set; } = null;
    }
    public class CloudVolume
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public CloudVolumeType Type { get; set; }
    }

    public enum CloudVolumeType
    {
        BlockStorage,
        VirtualStorage
    }
}